# recycled_3R
Reciclados 3R - Sistema de Gestión de Notas

Descripción

Este proyecto es un sistema de gestión de notas para la empresa "Reciclados 3R". Permite a los usuarios de diferentes departamentos (como Atención al Cliente, Recursos Humanos, etc.) gestionar notas creadas a partir de llamadas telefónicas de clientes. El sistema soporta operaciones de CRUD (Crear, Leer, Actualizar, Eliminar) con un enfoque en la seguridad y asignación de roles y permisos.

Características

Autenticación de Usuarios: Login para diferentes roles y departamentos.
Gestión de Notas: Permite crear, leer, actualizar y eliminar notas con restricciones basadas en roles y departamentos.
Seguridad: Control de acceso basado en roles y departamentos.
Interfaz de Usuario: Interfaz intuitiva para la gestión de notas.

Requisitos Previos

Servidor Apache/Nginx
PHP (versión recomendada: 7.4 o superior)
MySQL (versión recomendada: 5.7 o superior)

Instalación y Configuración
Clonar el Repositorio:
git clone https://gitlab.com/AngelloDeveloper/recycled_3r.git

Configuración de la Base de Datos:

Crear una base de datos en MySQL.
Importar el esquema de la base de datos usando los archivos *.sql proporcionados.
Configuración del Proyecto:
Configurar la conexión a la base de datos en el archivo config.php.
Ajustar cualquier configuración específica del entorno en el archivo de configuración.

Uso
Iniciar Sesión: Acceda a la aplicación utilizando las credenciales de usuario.

Gestión de Notas:

Crear Nota: Disponible solo para usuarios del departamento de Atención al Cliente.
Leer Notas: Cada departamento puede ver sus propias notas.
Actualizar Notas: Restricciones basadas en roles y creador de la nota.
Eliminar Notas: Limitado a roles específicos en el departamento de Atención al Cliente.

Desarrollo y Pruebas

Se recomienda utilizar un entorno de desarrollo local como XAMPP o WAMP.
Ejecutar pruebas unitarias y de integración para asegurar la funcionalidad.
Contribuciones
Las contribuciones son bienvenidas. Por favor, lea el archivo CONTRIBUTING.md para más detalles sobre cómo contribuir al proyecto.

Licencia
Este proyecto está licenciado bajo [Nombre de la Licencia]. Vea el archivo LICENSE para más detalles.

Contacto
Desarrollador: Angello Durán
Correo Electrónico: angelloprogramador@gmail.com